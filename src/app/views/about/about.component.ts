import { Component, OnInit } from '@angular/core';
import { TeamMember } from 'src/app/models/team-member.model';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  team: TeamMember[] = [
    {
      name: "Sergio Cerdá Hervás", position: "Main Web developer", portrait: "./assets/portraits/sergio.jpeg",
      description: "Software Engineer graduated in Computer Science", 
      social: [
        {name: 'linkedin', class: "fa fab fa-linkedin", href: "https://www.linkedin.com/in/sergio-cerda-hervas/"},
        {name: "gitlab", class: "fa fab fa-gitlab",  href: "https://gitlab.com/Kirvo"}
      ]
    },
    {
      name: "Ferran Ramírez Martí", position: "Apps Implementation and web development", portrait: "./assets/portraits/ferran.jpeg",
      description: "Computing Engineer graduated in Computer Science", 
      social: [
        {name: 'linkedin', class: "fa fab fa-linkedin", href: "https://www.linkedin.com/in/ferran-ramirez/"},
        {name: "gitlab", class: "fa fab fa-gitlab",  href: "https://gitlab.com/manyNeurons"}
      ]
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
