import { Project } from './../../models/project.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  projectList: Project[] = [
    { name: "Cellular Stuff",
     description: "Visual, interactive and educative representation and explanation of a One-dimensional Cellular Automaton.", 
     author:"Ferran Ramírez Martí", 
     urlProject: "https://cellular-stuff.netlify.app", 
     urlSource: "https://gitlab.com/fewNeurons/cellular-automaton-web",
     icon: "../../../assets/icons/celula.png",
     image: "../../../assets/samples/cellular-stuff-preview.png"},
    { 
      name: "Rock, Paper, Sockets!", 
      description: "This is a small project with I have learned to use websockets and work with Node, Socket.io and other technologies.",
      author:"Sergio Cerdá Hervás", 
      urlProject: "https://rock-paper-sockets.netlify.app/", 
      urlSource: 'https://gitlab.com/Kirvo/rock-paper-sockets',
      icon: "../../../assets/icons/rock-paper-sockets.png", 
      image: "../../../assets/samples/rock-paper-sockets.png"
    },
    {
      name: "Coming soon...",  icon: "../../../assets/icons/comming-soon.jfif", image: "../../../assets/samples/coming-soon.png"
    }
  ];

  projectSelected: Project;

  constructor() { }

  ngOnInit(): void {
    this.projectSelected = this.projectList[0];
  }

  selectProject(project: Project) {
    console.log("Has seleccionado: ", project.name)
    this.projectSelected = project;
  }
}
